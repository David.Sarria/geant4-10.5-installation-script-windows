**DO NOT EXECUTE these .bat files if you don't want to rebuild Geant4 and dependencies from scratch**



## WINDOWS GEANT4 INSTALLATION SCRIPT

### Batch (.bat) scripts to compile and install GEANT4 on Windows with full functionallity (GDML, OpenGL, Qt). 
### EXPERIMENTAL: probably still a few bugs. It may also mess up the Environment Variables... Use at your own risk.

* Requires Visual Studio 2017. The installer is provided. Install with visual C++ support. **It is worth running the installer twice to make sure that the C++ support is installed correctly**.

* To compile and install everything, right click on `compile_install_Full.bat` and "Run as administrator". Will take at least 1.5 hour to compile and install... If everything works fine, it should compile and execute example B1. **If it crashes, try to execute again exampleB1.exe located at** `{git_root}/ExampleB1/build/Debug/`

* **It will create or update environement variables for the Geant4 datasets. So it could mess up your own Geant4 installation.**

* Use `launch_visual_studio.bat` to launch Visual Studio IDE with the correct environment. A deskop shorcut `Launch_Visual_Studio_for_Geant4` is also created.

* It will take a disk space of about 15 GB when finished. A lot of files are temporary compilation products and source code (only ~4.6 GB is mandatory).

## Extra information
* Compiles everything in *Debug mode* by default, but can be easily changed in the scripts. **Remark:** linking libraries compiled in *Debug* mode with main code compiled in *Release* mode does not work under Windows (even if it works under Linux environements).
* to remove some of the warnings at runtime :
  * PDB is a debug information file used by Visual Studio. Some system DLLs, which you don't have debug symbols for, will show warnings. In Visual Studio, go to Tools->Options->Debugging->Symbols and select checkbox "Microsoft Symbol Servers", Visual Studio will download PDBs automatically. Or you may just ignore these warnings if you don't need to see correct call stack in these modules.
