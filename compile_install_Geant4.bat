@echo off
cls
set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\compile_install_Geant4.txt
exit /b
:sub



REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

set G4_bat_file_dir=%~dp0
set build_type=Debug

set QTDIR=%G4_bat_file_dir%Qt5\install\
set QMAKESPEC=win32-msvc2017

set PATH=%PATH%;%QTDIR%bin;%QTDIR%lib;%QTDIR%plugins;%QTDIR%plugins\platforms

cd %G4_bat_file_dir%

REM Compile and install GEANT4

echo Compiling and Installing GEANT4 10_05_p01

if not exist %G4_bat_file_dir%\build mkdir %G4_bat_file_dir%\build

if not exist %G4_bat_file_dir%\install mkdir %G4_bat_file_dir%\install

cd %G4_bat_file_dir%build

REM if exist CMakeCache.txt del CMakeCache.txt

if %build_type%==Debug (set xerses_lib=xerces-c_3D.lib) else (set xerses_lib=xerces-c_3.lib)

REM -DGEANT4_FORCE_QT4=ON ^

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe ^
-DCMAKE_INSTALL_PREFIX=..\install ^
-DCMAKE_PREFIX_PATH=..\xerces-c\install\cmake;..\Qt5\install\lib\cmake ^
-DBUILD_SHARED_LIBS=OFF ^
-DBUILD_STATIC_LIBS=ON ^
-DGEANT4_INSTALL_DATA=ON ^
-DGEANT4_USE_QT=ON ^
-DQT_QMAKE_EXECUTABLE=..\Qt5\install\bin\qmake.exe ^
-DGEANT4_USE_OPENGL_WIN32=ON ^
-DGEANT4_USE_GDML=ON ^
-DXERCESC_INCLUDE_DIR=..\xerces-c\install\include ^
-DXERCESC_LIBRARY=..\xerces-c\install\lib\%xerses_lib% ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DGEANT4_BUILD_MSVC_MP=ON ^
-DGEANT4_INSTALL_EXAMPLES=OFF ^
-DGEANT4_USE_G3TOG4=ON ^
-DGEANT4_BUILD_CXXSTD=11 ^
-DGEANT4_BUILD_MULTITHREADED=OFF ^
%G4_bat_file_dir%\geant4_10_05_p01

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

cd %G4_bat_file_dir%

REM Setting up more environement variables
REM setx is for permanent (future commands)
REM set is local (only this command execution)

echo Setting environement variables

set PATH=%PATH%;%G4_bat_file_dir%install\bin;%QTDIR%bin;%G4_bat_file_dir%install\lib;%QTDIR%lib;%G4_bat_file_dir%install\include\Geant4

set G4dataset_RootDir=%G4_bat_file_dir%install\share\Geant4-10.5.1\data\

setx G4dataset_RootDir %G4_bat_file_dir%install\share\Geant4-10.5.1\data\

setx G4ABLADATA %%G4dataset_RootDir%%G4ABLA3.1
setx G4ENSDFSTATEDATA %%G4dataset_RootDir%%G4ENSDFSTATE2.2
setx G4INCLDATA %%G4dataset_RootDir%%G4INCL1.0
setx G4LEDATA %%G4dataset_RootDir%%G4EMLOW7.7
setx G4LEVELGAMMADATA %%G4dataset_RootDir%%PhotonEvaporation5.3
setx G4NEUTRONHPDATA %%G4dataset_RootDir%%G4NDL4.5
setx G4PARTICLEXSDATA %%G4dataset_RootDir%%G4PARTICLEXS1.1
setx G4PIIDATA %%G4dataset_RootDir%%G4PII1.3
setx G4RADIOACTIVEDATA %%G4dataset_RootDir%%RadioactiveDecay5.3
setx G4REALSURFACEDATA %%G4dataset_RootDir%%RealSurface2.1.1
setx G4SAIDXSDATA %%G4dataset_RootDir%%G4SAIDDATA2.0

set G4ABLADATA=%%G4dataset_RootDir%%G4ABLA3.1
set G4ENSDFSTATEDATA=%%G4dataset_RootDir%%G4ENSDFSTATE2.2
set G4INCLDATA=%%G4dataset_RootDir%%G4INCL1.0
set G4LEDATA=%%G4dataset_RootDir%%G4EMLOW7.7
set G4LEVELGAMMADATA=%%G4dataset_RootDir%%PhotonEvaporation5.3
set G4NEUTRONHPDATA=%%G4dataset_RootDir%%G4NDL4.5
set G4PARTICLEXSDATA=%%G4dataset_RootDir%%G4PARTICLEXS1.1
set G4PIIDATA=%%G4dataset_RootDir%%G4PII1.3
set G4RADIOACTIVEDATA=%%G4dataset_RootDir%%RadioactiveDecay5.3
set G4REALSURFACEDATA=%%G4dataset_RootDir%%RealSurface2.1.1
set G4SAIDXSDATA=%%G4dataset_RootDir%%G4SAIDDATA2.0

setx Geant4_DIR %G4_bat_file_dir%install\lib\Geant4-10.5.1\
set Geant4_DIR=%G4_bat_file_dir%install\lib\Geant4-10.5.1\

echo " "
echo "Done"

REM PAUSE
