@echo off
cls

set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\compile_install_CADMesh.txt
exit /b
:sub



REM setx is for permanent (future commands)
REM set is local (only this command execution)

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

REM set build_type=Release
REM set build_type=RelWithDebInfo
set build_type=Debig

set G4_bat_file_dir=%~dp0
echo %G4_bat_file_dir%

cd %G4_bat_file_dir%

IF EXIST %G4_bat_file_dir%\cmake-3.14\bin SET PATH=%PATH%;%G4_bat_file_dir%\cmake-3.14\bin

REM Compile and install CADMesh
echo Compiling and Installing CADMesh
cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%\CADMesh\install mkdir %G4_bat_file_dir%\CADMesh\install
if not exist %G4_bat_file_dir%\CADMesh\build mkdir %G4_bat_file_dir%\CADMesh\build

cd %G4_bat_file_dir%\CADMesh\build

if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe -DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\CADMesh\install ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DBUILD_SHARED_LIBS=OFF ^
-DBUILD_STATIC_LIBS=ON ^
%G4_bat_file_dir%\CADMesh\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

setx cadmesh_DIR %G4_bat_file_dir%\CADMesh\install\lib\cmake\cadmesh-1.1.0
setx assimp_DIR %G4_bat_file_dir%\install\lib\cmake\assimp-3.1
setx cadmesh_include %G4_bat_file_dir%\CADMesh\install\include\

echo " "
echo "Done"

REM PAUSE
