@echo off
cls

set G4_bat_file_dir=%~dp0
call :sub >%G4_bat_file_dir%\logs\compile_install_Qt5.txt
exit /b
:sub

REM setx is for permanent (future commands)
REM set is local (only this command execution)

REM loading visual studio environment
set G4_bat_file_dir=%~dp0
cd %G4_bat_file_dir%
call vsenv.bat

REM Set temporary environement varialbe PATH to find perl and CMake

IF EXIST %G4_bat_file_dir%\strawberry-perl\perl\bin SET PATH=%PATH%;%G4_bat_file_dir%\strawberry-perl\perl\bin
IF EXIST %G4_bat_file_dir%\cmake-3.14\bin SET PATH=%PATH%;%G4_bat_file_dir%\cmake-3.14\bin

where /q perl || ECHO Cound not find perl. Aborting. && PAUSE && EXIT /B

where /q cmake || ECHO Cound not find cmake. Aborting. && PAUSE && EXIT /B

REM Compile and install Qt5 5.6.3
echo Compiling and Installing Qt5 5.6.3
cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%Qt5\install mkdir %G4_bat_file_dir%Qt5\install

set QTDIR=%G4_bat_file_dir%Qt5\install\
set QMAKESPEC=win32-msvc2017

setx QTDIR %G4_bat_file_dir%Qt5\install\
setx QMAKESPEC win32-msvc2017

cd %G4_bat_file_dir%Qt5\qt-5.6.3\

set PATH=%PATH%;%QTDIR%bin;%QTDIR%lib

REM -debug-and-release

nmake distclean

configure -platform %QMAKESPEC% ^
-opensource -confirm-license -opengl desktop -no-pch ^
-debug-and-release -mp -force-debug-info -nomake examples -make libs -make tools ^
-prefix %QTDIR% -nomake tests -no-compile-examples

nmake
nmake install

REM -static
REM -debug

cd %G4_bat_file_dir%Qt5\qttools-5.6.3

qmake -o Makefile qttools.pro

nmake
nmake install

echo " "
echo "DONE !"

REM PAUSE
